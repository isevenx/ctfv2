#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define FEAT_FACE_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_frontalface_alt.xml"


#include "options.h"
#include "worker.h"
#include "idgen.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QVector>
#include <QDebug>
#include <QMetaObject>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QCoreApplication>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void findFiles(QDir path, QString indent);

    QString dirName,spath;
    QStringList fileName;
    QDir currentDir;
    QFileInfoList list,files;
    QList<QFileInfo> info;

    int hasjpg,faceCount,curPic,Width,Height,BnW,tC;


    Options optionW;
    IDGen id;

    //worker Worker;

    vector<worker*> workList;
    //vector<int> test2;

    struct config{
        QString rFolder;        
        int ID,Width,Height,BnW;
    }Config;




    ~MainWindow();

signals:    

    void startWork(int who,QFileInfo fInfo);

public slots:
    void OK(int who);
    void chooseFolder1();
    void chooseFolder2();
    void makePictures();
    void findPictures();
    void findDir();
    void faceDetect(int picNum);
    void oShow();
    void changeOptions(int width,int height,int bnw);    

    void continueWork(int who);


private:



    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
