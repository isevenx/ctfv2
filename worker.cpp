#include "worker.h"

worker::worker(QObject *parent) :
    QObject(parent)
{
}

void worker::getInfo(config conf){
    Config=conf;
}
void worker::iHPicNum(int fc){
    faceCount=fc;
}

void worker::getRequest(int who,QFileInfo fInfo)
{
    if(who==ID) faceCapture(fInfo);
}

void worker::faceCapture(QFileInfo fInfo)
{

    cv::Mat mOrigImage = cv::imread(fInfo.filePath().toStdString());
    faceCount=0;
    QString qfileName;
    cv::Mat mElabImage;
    mOrigImage.copyTo( mElabImage );
    cv::CascadeClassifier mFaceDetector;
    if( mFaceDetector.empty() )
        mFaceDetector.load( FEAT_FACE_FILE );

    vector<cv::Rect> faceVec;

    float scaleFactor = 1.1f; // Change Scale Factor to change speed
    mFaceDetector.detectMultiScale(mOrigImage,faceVec,scaleFactor);    

    for( size_t i=0; i<faceVec.size(); i++ )
    {
        cv::rectangle( mElabImage, faceVec[i], CV_RGB(255,0,0), 2 );
        cv::Mat face = mOrigImage( faceVec[i] );
        emit iWPicNum();
        cv::Size size(Config.Width,Config.Height);
        cv::Mat rFace;
        //cv::namedWindow("Face");
        //cv::imshow( "Face", face );
        qfileName=Config.rFolder+fInfo.fileName();
        qfileName.chop(4);
        if ((Config.BnW==2) || (Config.BnW==3)) qfileName=Config.rFolder+QString::number(faceCount)+".jpg";
        else qfileName+="_"+QString::number(i+1)+".jpg";
        if((Config.BnW==1) || (Config.BnW==3)) cvtColor(face,face,CV_BGR2GRAY);        
        cv::resize(face,rFace,size);
        cv::imwrite(qfileName.toStdString(),rFace);
    }
        //cv::namedWindow("My Image",WINDOW_NORMAL);
        //cv::imshow("My Image", mElabImage);

    qDebug() << ID;
    emit Done(ID);
}
