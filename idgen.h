#ifndef IDGEN_H
#define IDGEN_H

#include <QObject>
#include <QDebug>

class IDGen : public QObject
{
    Q_OBJECT

public:
    explicit IDGen(QObject *parent = 0);

    int faces,tPic,wwPic;

signals:

    void endOfProcess(int inc);
    void tYPicNum(int fc);


public slots:

    void iwpcGot();

};

#endif // IDGEN_H
