#ifndef WORKER_H
#define WORKER_H
#define FEAT_FACE_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_frontalface_alt.xml"

#include <QObject>
#include <QMainWindow>
#include <QDebug>
#include <QFileInfo>
#include <QFileDialog>
#include <QVector>
#include <QDebug>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QCoreApplication>

using namespace std;
using namespace cv;

class worker : public QObject
{
    Q_OBJECT
public:
    struct config{
        QString rFolder;        
        int ID,Width,Height,BnW;
    }Config;
    int faceCount,ID;

    explicit worker(QObject *parent = 0);
    //explicit worker(const worker& other);


signals:
    void Done(int who);
    void iWFC();
    void iWPicNum();

public slots:
    void getRequest(int who,QFileInfo fInfo);
    void getInfo(config Config);
    void faceCapture(QFileInfo fInfo);
    void iHPicNum(int fc);
};

#endif // WORKER_H
