#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace cv;

void MainWindow::OK(int picC){
    ui->label_2->setText("Result:  "+QString::number(picC)+" processed, "+QString::number(id.faces)+" faces made");
}

void MainWindow::continueWork(int who){
    curPic++;
    //qDebug() << who << curPic;
    if(curPic<info.size())
        QMetaObject::invokeMethod(workList[who],"faceCapture",Qt::DirectConnection,Q_ARG(QFileInfo,info[curPic]));
    else emit OK(curPic);

}


void MainWindow::oShow(){     // show options window
   optionW.show();
}
void MainWindow::changeOptions(int width,int height,int bnw){
    Width=width;
    Height=height;
    BnW=bnw;
}

void MainWindow::findDir(){     // show file Dir
    for (int i=0;i<info.size();i++){
        if(ui->listWidget->currentItem()->text().endsWith(info[i].fileName())){
            ui->lineEdit->clear();
            ui->lineEdit->insert(info[i].filePath());
        }
    }
}
void MainWindow::chooseFolder2(){     // browse -2-
    dirName = QFileDialog::getExistingDirectory(this,
    tr("Result Directory"),"/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dirName.isEmpty()){
            ui->comboBox_2->addItem(dirName);
            int index = ui->comboBox_2->findText(dirName);
            ui->comboBox_2->setCurrentIndex(index);
    }
}
void MainWindow::chooseFolder1(){     // browse -1-
    dirName = QFileDialog::getExistingDirectory(this,
    tr("Search Directory"),"/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dirName.isEmpty()){
            ui->comboBox->addItem(dirName);
            int index = ui->comboBox->findText(dirName);
            ui->comboBox->setCurrentIndex(index);
    }
}


void MainWindow::faceDetect(int picNum)                   // -4-
{

    cv::Mat mOrigImage = cv::imread(info[picNum].filePath().toStdString());
    QString qfileName;
    cv::Mat mElabImage;
    mOrigImage.copyTo( mElabImage );
    cv::CascadeClassifier mFaceDetector;

    if( mFaceDetector.empty() )
        mFaceDetector.load( FEAT_FACE_FILE );

    vector<cv::Rect> faceVec;

    float scaleFactor = 1.1f; // Change Scale Factor to change speed
    mFaceDetector.detectMultiScale(mOrigImage,faceVec,scaleFactor);

    for( size_t i=0; i<faceVec.size(); i++ )
    {
        cv::rectangle( mElabImage, faceVec[i], CV_RGB(255,0,0), 2 );
        cv::Mat face = mOrigImage( faceVec[i] );
        faceCount++;
        cv::Size size(Width,Height);
        cv::Mat rFace;

        //cv::namedWindow("Face");
        //cv::imshow( "Face", face );
        qfileName=ui->comboBox_2->currentText()+"/"+info[picNum].fileName();
        qfileName.chop(4);
        if ((BnW==2) || (BnW==3)) qfileName=ui->comboBox_2->currentText()+"/"+QString::number(faceCount)+".jpg";
        else qfileName+="_"+QString::number(i+1)+".jpg";
        if((BnW==1) || (BnW==3)) cvtColor(face,face,CV_BGR2GRAY);
        cv::resize(face,rFace,size);
        cv::imwrite(qfileName.toStdString(),rFace);
    }
        //cv::namedWindow("My Image",WINDOW_NORMAL);
        //cv::imshow("My Image", mElabImage);

}
void MainWindow::makePictures()                           // -3-
{
    QString path=ui->comboBox_2->currentText()+"/";
    QDir dir(path);
    if(!dir.exists()) dir.mkpath(path);
    if(!info.isEmpty()){        
        for(int i=0;i<tC-1;i++){
            (*(workList[i])).faceCount=0;
            (*(workList[i])).ID=i;

        }
        Config.BnW=BnW; Config.Height=Height; Config.Width=Width;
        Config.rFolder=ui->comboBox_2->currentText()+"/";

        for(int i=0;i<tC-1;i++)
            QMetaObject::invokeMethod(workList[i],"getInfo",Qt::DirectConnection,Q_ARG(config, Config));

        for(int i=0;i<tC-1;i++)
            if(i<=info.size()){
                if(curPic<id.tPic) QMetaObject::invokeMethod(this,"startWork",Q_ARG(int, i),Q_ARG(QFileInfo, info[curPic]));
            }
    }    
}

void MainWindow::findFiles(QDir path, QString indent)     // -2-
{
    currentDir = QDir(path);
    indent += "           ";
    list = currentDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QFileInfo finfo, list) {
       if(finfo.fileName().endsWith(".jpg")|finfo.fileName().endsWith(".png")) {
           hasjpg++;
           ui->listWidget->addItem(indent+finfo.fileName());
           info << finfo;
       }
       if (finfo.isDir()&&ui->checkBox->isChecked()) {
           findFiles(QDir(finfo.absoluteFilePath()), indent);
       }
    }
}
void MainWindow::findPictures()                           // -1-
{
    ui->label_2->setText(" ");
    hasjpg=0;
    id.wwPic=0;
    id.faces=0;
    curPic=0;

    info.clear();
    ui->listWidget->clear();
    spath = ui->comboBox->currentText();
    findFiles(spath,"");
    id.tPic=hasjpg;
    if (ui->checkBox_2->isChecked()) {
        makePictures();        
    }
    else ui->label_2->setText("Result:  "+QString::number(hasjpg)+" found");
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    tC=4;
    worker *tw;
    for(int i=0;i<tC-1;i++){
        tw=new worker();
        workList.push_back(tw);
    }

    Width=70;
    Height=70;
    BnW=0;


    connect(ui->pushButton,SIGNAL(clicked()),SLOT(findPictures()));
    connect(ui->pushButton_2,SIGNAL(clicked()),SLOT(chooseFolder1()));
    connect(ui->pushButton_3,SIGNAL(clicked()),SLOT(chooseFolder2()));
    connect(ui->pushButton_4,SIGNAL(clicked()),SLOT(oShow()));
    connect(ui->listWidget,SIGNAL(itemClicked(QListWidgetItem*)),SLOT(findDir()));
    connect(&optionW,SIGNAL(sendOptions(int,int,int)),SLOT(changeOptions(int,int,int)));

    for(int i=0;i<tC-1;i++){
        connect(this,SIGNAL(startWork(int,QFileInfo)),workList[i],SLOT(getRequest(int,QFileInfo)));
        connect(workList[i],SIGNAL(Done(int)),this,SLOT(continueWork(int)));
        connect(workList[i],SIGNAL(iWPicNum()),&id,SLOT(iwpcGot()));
        connect(&id,SIGNAL(tYPicNum(int)),workList[i],SLOT(iHPicNum(int)));
    }

    ui->comboBox->addItem("/home/rihard/Pictures");
    ui->comboBox_2->addItem("/home/rihard/Result");

}
MainWindow::~MainWindow()
{
    delete ui;

}
